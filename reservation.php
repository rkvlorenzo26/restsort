<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3>Pending Reservations</h3>
                                    <hr>
                                    <table id="tablePendingReservation" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>CHECK-IN DATE</th>
                                                <th>CHECK-OUT DATE</th>
                                                <th>CUSTOMER</th>
                                                <th>CONTACT NO.</th>
                                                <th>EMAIL</th>
                                                <th>ROOM</th>
                                                <th>INSTRUCTIONS</th>
                                                <th>STATUS</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_reservation = mysqli_query($con, "SELECT reservation.id, reservation.reservation_date_from, reservation.reservation_date_to, user.firstname, 
                                                    user.lastname, user.contact_no, user.email, rates.name, reservation.instructions, reservation.status FROM tbl_resort_reservation reservation
                                                    LEFT JOIN tbl_users user ON reservation.user_id = user.user_id
                                                    LEFT JOIN tbl_resort_rates rates ON reservation.rate_id = rates.rate_id
                                                    WHERE reservation.resort_id = '$_SESSION[resort_id]' AND reservation.status = 'Pending' GROUP BY reservation.id");
                                            while($row = mysqli_fetch_array($get_reservation))
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['reservation_date_from']; ?></td>
                                                <td><?php echo $row['reservation_date_to']; ?></td>
                                                <td><?php echo $row['firstname']. ' ' .$row['lastname'];?></td>
                                                <td><?php echo $row['contact_no'];?></td>
                                                <td><?php echo $row['email'];?></td>
                                                <td><?php echo $row['name'];?></td>
                                                <td><?php echo $row['instructions'];?></td>
                                                <td><?php echo $row['status'];?></td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" onclick="action(<?php echo $row['id']; ?>, 'Approved')" data-toggle="tooltip" data-placement="left" title="Approve" id="approve" name="approve"><span class="fa fa-check"></span></a>
                                                    <a href="javascript:void(0)" onclick="action(<?php echo $row['id']; ?>, 'Declined')" data-toggle="tooltip" data-placement="left" title="Decline" id="decline" name="decline"><span class="fa fa-times"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3>Active Reservations</h3>
                                    <hr>
                                    <table id="tableActiveReservation" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>CHECK-IN DATE</th>
                                                <th>CHECK-OUT DATE</th>
                                                <th>CUSTOMER</th>
                                                <th>CONTACT NO.</th>
                                                <th>EMAIL</th>
                                                <th>ROOM</th>
                                                <th>INSTRUCTIONS</th>
                                                <th>STATUS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_reservation = mysqli_query($con, "SELECT reservation.id, reservation.reservation_date_from, reservation.reservation_date_to, 
                                                    user.firstname, user.lastname, user.contact_no, user.email, rates.name, reservation.instructions, reservation.status FROM tbl_resort_reservation reservation
                                                    LEFT JOIN tbl_users user ON reservation.user_id = user.user_id
                                                    LEFT JOIN tbl_resort_rates rates ON reservation.rate_id = rates.rate_id
                                                    WHERE reservation.resort_id = '$_SESSION[resort_id]' AND reservation.status = 'Approved' GROUP BY reservation.id");
                                            while($row = mysqli_fetch_array($get_reservation))
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['reservation_date_from']; ?></td>
                                                <td><?php echo $row['reservation_date_to']; ?></td>
                                                <td><?php echo $row['firstname']. ' ' .$row['lastname'];?></td>
                                                <td><?php echo $row['contact_no'];?></td>
                                                <td><?php echo $row['email'];?></td>
                                                <td><?php echo $row['name'];?></td>
                                                <td><?php echo $row['instructions'];?></td>
                                                <td><?php echo $row['status'];?></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="confirmModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p id="confimHeader"></p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <p id="confirmMessage"></p>
                <form action="functions/php_functions/update_reservation.php" method="POST">
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="status" name="status">
                    <p name="reasonText" id="reasonText">Reason for declining:<p/>
                    <textarea class="form-control" id="reason" name="reason"></textarea>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php include_once 'include_once/js.php'?>

    <script>
        function action(id, action) {
            if (action == "Approved") {
                $('#confimHeader').text("Approve Reservation");
                $('#id').val(id);
                $('#status').val(action);
                $('#reasonText').hide();
                $('#reason').hide();
                $('#confirmMessage').text("Are you sure that you want to approve this reservation? (This can't be undone)");
                $('#confirmModal').modal('show');
            } else if (action == "Declined") {
                $('#confimHeader').text("Decline Reservation");
                $('#id').val(id);
                $('#status').val(action);
                $('#reasonText').show();
                $('#reason').show();
                $('#confirmMessage').text("Are you sure that you want to decline this reservation? (This can't be undone)");
                $('#confirmModal').modal('show');
            } else if ( action == "Add") {
                $('#addModal').modal('show');
            }
        }
    </script>

    <script>
        $(document).ready(function() {
            $('#tablePendingReservation').DataTable( {
                order: [[ 0, "desc" ]],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        title: '',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5] //Your Colume value those you want
                        },
                        customize: function( win ) {
                            var now = new Date();
                            var jsDate = (now.getMonth()+1)+'-'+now.getDate()+'-'+now.getFullYear();
                            $(win.document.body)
                                .prepend('<div style="position:absolute; top:70; right:0;"><h5>Date Generated: ' + jsDate +'</h5></div>')
                                .prepend('<div style="position:absolute; top:50; right:0;"><h5>Generated by: <?php echo $_SESSION['username']; ?></h5></div>')
                                .prepend('</br></br></br></br></br></br><div><center><h2>Pending Reservations</h2></center></div>');
                        }
                    }
                ]
            } );

            $('#tableActiveReservation').DataTable( {
                order: [[ 0, "desc" ]],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        title: '',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5] //Your Colume value those you want
                        },
                        customize: function( win ) {
                            var now = new Date();
                            var jsDate = (now.getMonth()+1)+'-'+now.getDate()+'-'+now.getFullYear();
                            $(win.document.body)
                                .prepend('<div style="position:absolute; top:70; right:0;"><h5>Date Generated: ' + jsDate +'</h5></div>')
                                .prepend('<div style="position:absolute; top:50; right:0;"><h5>Generated by: <?php echo $_SESSION['username']; ?></h5></div>')
                                .prepend('</br></br></br></br></br></br><div><center><h2>Active Reservations</h2></center></div>');
                        }
                    }
                ]
            } );
        } );
    </script>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>