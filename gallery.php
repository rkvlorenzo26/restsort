<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <a style="margin-bottom: 10px;" href="javascript:void(0)" class="btn btn-primary" onclick="action('', 'Add')">Upload new image</a>
                                    <table id="tablePosts" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th style="width: 25%;">IMAGE</th>
                                                <th style="width: 15%;">TITLE</th>
                                                <th>DESCRIPTION</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_gallery = mysqli_query($con, "SELECT * FROM tbl_resort_gallery WHERE resort_id = '$_SESSION[resort_id]'");
                                            while($row = mysqli_fetch_array($get_gallery))
                                            {
                                            ?>
                                            <tr>
                                                <td><img style="width: 100%;" src="uploads/gallery/<?php echo $row['image']; ?>"></td>
                                                <td><?php echo $row['title'];?></td>
                                                <td><?php echo $row['description'];?></td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" onclick="edit(<?php echo $row['gallery_id']; ?>, '<?php echo $row['title']; ?>', '<?php echo $row['description']; ?>')" data-toggle="tooltip" data-placement="left" title="Edit" id="edit" name="edit"><span class="fa fa-edit"></span></a>
                                                    <a href="javascript:void(0)" onclick="action(<?php echo $row['gallery_id']; ?>, 'Delete')" data-toggle="tooltip" data-placement="left" title="Delete" id="delete" name="delete"><span class="fa fa-trash"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="confirmModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p id="confimHeader"></p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <p id="confirmMessage"></p>
                <form action="functions/php_functions/gallery_delete.php" method="POST">
                    <input type="hidden" id="gallery_id" name="gallery_id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="addModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p>Upload new image</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <form action="functions/php_functions/gallery_add.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" id="title" name="title" class="form-control" placeholder="Title" required>
                    </div>

                    <div class="form-group">
                        <input type="text" id="description" name="description" class="form-control" placeholder="Description" required>
                    </div>

                    <div class="form-group">
                        <input type="file" id="fileToUpload" name="fileToUpload" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button name="submit" class="btn btn-primary">Upload</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="editModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p>Edit image</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <form action="functions/php_functions/gallery_update.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="edit_id" name="edit_id">
                    <div class="form-group">
                        <input type="text" id="edit_title" name="edit_title" class="form-control" placeholder="Title" required>
                    </div>

                    <div class="form-group">
                        <input type="text" id="edit_description" name="edit_description" class="form-control" placeholder="Description" required>
                    </div>

                    <div class="form-group">
                        <input type="file" id="fileToUpload" name="fileToUpload" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button name="submit" class="btn btn-primary">Upload</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php include_once 'include_once/js.php'?>

    <script>
        function action(id, action) {
            if (action == "Delete") {
                $('#confimHeader').text("Delete Message");
                $('#gallery_id').val(id);
                $('#confirmMessage').text("Are you sure that you want to delete this? (This can't be undone)");
                $('#confirmModal').modal('show');
            } else if ( action == "Add") {
                $('#addModal').modal('show');
            }
        }

        function edit(id, title, description) {
            $('#edit_id').val(id);
            $('#edit_title').val(title);
            $('#edit_description').val(description);
            $('#editModal').modal('show');
        }
    </script>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>