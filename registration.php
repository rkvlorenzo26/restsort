<?php 
include_once 'include_once/connection.php';
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/login/registration-header.php'; ?>
<body>
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
                <img src="include_once/restsort.png" style="width: 150px; height: 150px; margin-bottom: 15px;"></br>
                Registration Form
            </div>
            <div class="card-body">
                <div class="form-group">
                    Resort Name
                    <input class="form-control form-control-lg" id="resort" name="resort" type="text" placeholder="Resort Name">
                </div>
                <div class="form-group">
                    Address
                    <input class="form-control form-control-lg" id="address" name="address" type="text" placeholder="Address">
                </div>
                <div class="form-group">
                City/Municipality:
                    <select class="form-control" id="city_municipality" name="city_municipality">
                    <?php
                        $get_city_municipality = mysqli_query($con, "SELECT * FROM tbl_location ORDER BY name ASC");
                        $get_info = mysqli_query($con, "SELECT * FROM tbl_resort_info WHERE resort_id = '$_SESSION[resort_id]'");
                        $info = mysqli_fetch_array($get_info);
                        while ($row = mysqli_fetch_array($get_city_municipality)) {
                            if ($info['city_municipality'] == $row['name']) { ?>
                                <option value = "<?php echo $row['name']; ?>" selected><?php echo $row['name']; ?></option>
                    <?php   } else  {   ?>
                                <option value = "<?php echo $row['name']; ?>"><?php echo $row['name']; ?></option>
                    <?php   }   }   ?>
                    </select>
                </div>
                <div class="form-group">
                    Username
                    <input class="form-control form-control-lg" id="username" name="username" type="text" placeholder="Username" autocomplete="off">
                </div>
                <div class="form-group">
                    Email Address
                    <input class="form-control form-control-lg" id="email" name="email" type="email" placeholder="Email Address" autocomplete="off">
                </div>
                <div class="form-group">
                    Password
                    <input class="form-control form-control-lg" id="password" name="password" type="password" placeholder="Password">
                </div>
                <div class="form-group">
                    Re-type Password
                    <input class="form-control form-control-lg" id="retypepassword" name="retypepassword" type="password" placeholder="Re-type Password">
                </div>
                <button id="login" class="btn btn-lg btn-block btn-primary">Register</button>
            </div>
        </div>
    </div>
<?php include_once 'include_once/modal-alert.php'?>
<?php include_once 'include_once/modal-success.php'?>
<?php include_once 'include_once/login/registration-scripts.php'; ?>
</body>
</html>