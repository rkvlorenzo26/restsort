<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <a style="margin-bottom: 10px;" href="javascript:void(0)" class="btn btn-primary" onclick="action('', 'Add')">New Rate</a>
                                    <table id="tablePosts" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th style="width: 25%;">IMAGE</th>
                                                <th style="width: 15%;">TITLE</th>
                                                <th>DESCRIPTION</th>
                                                <th>PRICE</th>
                                                <th>STATUS</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_rates = mysqli_query($con, "SELECT * FROM tbl_resort_rates WHERE resort_id = '$_SESSION[resort_id]'");
                                            while($row = mysqli_fetch_array($get_rates))
                                            {
                                            ?>
                                            <tr>
                                                <td><img style="width: 100%;" src="uploads/rates/<?php echo $row['image']; ?>"></td>
                                                <td><?php echo $row['name'];?></td>
                                                <td><?php echo $row['description'];?></td>
                                                <td><?php echo $row['price'];?></td>
                                                <td><?php echo $row['status'];?></td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" onclick="edit(<?php echo $row['rate_id']; ?>, '<?php echo $row['name']; ?>', '<?php echo $row['description']; ?>', '<?php echo $row['price']; ?>')" data-toggle="tooltip" data-placement="left" title="Edit" id="edit" name="edit"><span class="fa fa-edit"></span></a>
                                                    <a href="javascript:void(0)" onclick="action(<?php echo $row['rate_id']; ?>, 'Delete')" data-toggle="tooltip" data-placement="left" title="Delete" id="delete" name="delete"><span class="fa fa-trash"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="confirmModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p id="confimHeader"></p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <p id="confirmMessage"></p>
                <form action="functions/php_functions/rates_delete.php" method="POST">
                    <input type="hidden" id="rate_id" name="rate_id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary">Yes</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="addModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p>New Rate</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <form action="functions/php_functions/rates_add.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
                    </div>

                    <div class="form-group">
                        <input type="text" id="description" name="description" class="form-control" placeholder="Description" required>
                    </div>

                    <div class="form-group">
                        <input type="number" id="price" name="price" class="form-control" placeholder="Price" required>
                    </div>

                    <div class="form-group">
                        <input type="file" id="fileToUpload" name="fileToUpload" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button name="submit" class="btn btn-primary">Upload</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="editModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p>Edit Rate</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <form action="functions/php_functions/rates_edit.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" id="edit_id" name="edit_id">
                    <div class="form-group">
                        <input type="text" id="edit_name" name="edit_name" class="form-control" placeholder="Name" required>
                    </div>

                    <div class="form-group">
                        <input type="text" id="edit_description" name="edit_description" class="form-control" placeholder="Description" required>
                    </div>

                    <div class="form-group">
                        <input type="number" id="edit_price" name="edit_price" class="form-control" placeholder="Price" required>
                    </div>

                    <div class="form-group">
                        <select id="status" name="status" class="form-control" required>
                            <option>Available</option>
                            <option>Not Available</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="file" id="fileToUpload" name="fileToUpload" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button name="submit" class="btn btn-primary">Upload</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php include_once 'include_once/js.php'?>

    <script>
        function action(id, action) {
            if (action == "Delete") {
                $('#confimHeader').text("Delete Message");
                $('#rate_id').val(id);
                $('#confirmMessage').text("Are you sure that you want to delete this? (This can't be undone)");
                $('#confirmModal').modal('show');
            } else if ( action == "Add") {
                $('#addModal').modal('show');
            }
        }

        function edit(id, name, description, price) {
            $('#edit_id').val(id);
            $('#edit_name').val(name);
            $('#edit_price').val(price);
            $('#edit_description').val(description);
            $('#editModal').modal('show');
        }
    </script>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>