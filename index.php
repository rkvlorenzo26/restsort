<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-10">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-section" id="overview">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="section-block" id="card-variance">
                                                <h3 class="section-title"> Overview </h3>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h3 id="resort-name" class="card-title"></h3>
                                                    <h6 id="resort-location" class="card-subtitle mb-2 text-muted border-bottom pb-2"></h6>
                                                    <p class="card-text" id="resort-info"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-section" id="rates">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="section-block" id="card-variance">
                                                <h3 class="section-title"> Rates </h3>
                                            </div>
                                        </div>

                                        <?php
                                            $get_rates = mysqli_query($con, "SELECT * FROM tbl_resort_rates WHERE resort_id = '$_SESSION[resort_id]'");
                                            while ($row = mysqli_fetch_array($get_rates)) { ?>
                                    
                                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="card">
                                                    <img style="height: 200px;" class="card-img-top img-fluid" src="uploads/rates/<?php echo $row['image']; ?>" alt="Card image cap">
                                                    <div class="card-body">
                                                        <h3 class="card-title"><?php echo $row['name']; ?></h3>
                                                        <p class="card-text">Php <?php echo $row['price']; ?></p>
                                                        <p class="card-text"><?php echo $row['description']; ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php    }   ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-section" id="gallery">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="section-block" id="card-variance">
                                                <h3 class="section-title"> Gallery </h3>
                                            </div>
                                        </div>

                                        <?php
                                            $get_gallery = mysqli_query($con, "SELECT * FROM tbl_resort_gallery WHERE resort_id = '$_SESSION[resort_id]'");
                                            while ($row = mysqli_fetch_array($get_gallery)) {?>

                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <img class="card-img-top img-fluid" src="uploads/gallery/<?php echo $row['image']; ?>" alt="<?php echo $row['title']; ?>">
                                                <div class="card-body">
                                                    <h3 class="card-title"><?php echo $row['title']; ?></h3>
                                                    <p class="card-text"></p>
                                                </div>
                                            </div>
                                        </div>

                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                        <div class="sidebar-nav-fixed">
                            <ul class="list-unstyled">
                                <li><a href="#overview" class="active">Overview</a></li>
                                <li><a href="#rates">Rates</a></li>
                                <li><a href="#gallery">Gallery</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once 'include_once/js.php'?>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>