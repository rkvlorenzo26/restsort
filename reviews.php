<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tablePosts" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th style="width: 25%;">CUSTOMER NAME</th>
                                                <th style="width: 15%;">RATING</th>
                                                <th>COMMENT</th>
                                                <th>DATE REVIEWED</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_reviews = mysqli_query($con, "SELECT * FROM tbl_reviews JOIN tbl_users ON tbl_reviews.user_id = tbl_users.user_id WHERE resort_id = '$_SESSION[resort_id]' ORDER BY date_reviewed DESC");
                                            while($row = mysqli_fetch_array($get_reviews))
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['firstname'] . " " . $row['lastname'];?></td>
                                                <td><?php echo $row['ratings'];?></td>
                                                <td><?php echo $row['comment'];?></td>
                                                <td><?php echo $row['date_reviewed'];?></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once 'include_once/js.php'?>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>