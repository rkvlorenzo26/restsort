<?php
include_once '../include_once/connection.php';

$result = array();

if (isset($_POST['resort_id'])) {
    $resort_id = $_POST['resort_id'];
    $user_id = $_POST['user_id'];
    $review = $_POST['review'];
    $ratings = $_POST['ratings'];
    $image = $_POST['image'];
    $imgName = "";

    if ($image != "") {
        $imgName = $imgName . $resort_id."_".$user_id."_".time().".JPG";
        $decodedImage = base64_decode("$image");
        $return = file_put_contents("../uploads/reviews/".$imgName, $decodedImage);
    }

    $check_reservation = mysqli_query($con, "SELECT * FROM tbl_resort_reservation WHERE user_id = '$user_id' AND resort_id = '$resort_id' AND status = 'Approved'");
    $count_reservation = mysqli_num_rows($check_reservation);

    if ($count_reservation > 0) {
        $insert = mysqli_query($con, "INSERT INTO tbl_reviews(resort_id, user_id, ratings, comment, image) VALUES ('$resort_id', '$user_id', '$ratings', '$review', '$imgName')");
        if ($insert) {
            $result['status'] = "success";
            $result['message'] = "Review successfully added.";
        } else {
            $result['status'] = "failed";
            $result['message'] = "Unable to add reviews. Please try again.";
        }
    } else {
        $result['status'] = "failed";
        $result['message'] = "Unable to add reviews. You don't have approved reservation for this resort.";
    }
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable to add reviews. Please try again.";
}

echo json_encode($result);
?>