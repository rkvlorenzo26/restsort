<?php 
include_once '../include_once/connection.php';

$result = array();

if (isset($_POST['firstname']) && isset($_POST['lastname']) && $_POST['contact'] && isset($_POST['email']) && isset($_POST['password'])) {
    
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $contact = $_POST['contact'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    
    $check = mysqli_query($con, "SELECT * FROM tbl_users WHERE email = '$email'");
    if (mysqli_num_rows($check) == 0) {
        $code = md5($_POST['email']);
        $insert_user = mysqli_query($con, "INSERT into tbl_users(firstname, lastname, contact_no, email, password, code, status) values ('$firstname', '$lastname', '$contact', '$email', '$password', '$code', 'inactive')");
        if ($insert_user) {
            $link = 'https://restsort2020.000webhostapp.com/verify.php?code='.$code;

            // the message
            $msg = '
                <p>Hello '.$firstname.',</p>
                <p>Please click the link below to verify your account.</p>
                <a href='. $link .'>Verify your account here.</a>
                <p>If not working please go to this link:</p>
                <p>'.$link.'</p>
            ';
            
            // use wordwrap() if lines are longer than 70 characters
            $msg = wordwrap($msg,70);
            
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            
            // send email
            mail($email,"Restsort Account Verification",$msg,$headers);
            $result['status'] = "success";
            $result['message'] = "Account successfully registered.";
        } else {
            $result['status'] = "failed";
            $result['message'] = "Unable to register please try again.";
        }
    } else if (mysqli_num_rows($check) > 0) {
        $result['status'] = "failed";
        $result['message'] = "Email address already exists.";
    } else {
        $result['status'] = "failed";
        $result['message'] = "Unable to register please try again.";
    }
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable to register please try again.";
}
echo json_encode($result);
?>