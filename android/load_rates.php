<?php
include_once '../include_once/connection.php';

if (isset($_POST['resort_id'])) {
    $resort_id = $_POST['resort_id'];

    $result = array();
    $get_rates = mysqli_query($con, "SELECT * FROM tbl_resort_rates WHERE resort_id = '$resort_id'");

    $rates_arr = array();
    while ($row = mysqli_fetch_array($get_rates)) {
        $rates_arr[] = array(
            'rate_id' => $row['rate_id'],
            'name' => $row['name'],
            'description' => $row['description'],
            'price' => $row['price'],
            'image' => $row['image'],
            'status' => $row['status']
        );
    }
    $result['status'] = "success";
    $result['message'] = "Rates successfully retrieved.";
    $result['data'] = $rates_arr;
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable to retrieve resort rates.";
}

echo json_encode($result);
?>