<?php
include_once '../include_once/connection.php';

if (isset($_POST['resort_id'])) {
    $resort_id = $_POST['resort_id'];

    $get_info = mysqli_query($con, "SELECT * FROM tbl_resort_info WHERE resort_id = '$resort_id'");
    $info = mysqli_fetch_array($get_info);
    $info_arr = array(
        'name' => $info['name'],
        'location' => $info['location'],
        'city_municipality' => $info['city_municipality'],
        'description' => $info['description']
    );

    $result = array();
    $get_gallery = mysqli_query($con, "SELECT * FROM tbl_resort_gallery WHERE resort_id = '$resort_id'");

    $gallery_arr = array();
    while ($row = mysqli_fetch_array($get_gallery)) {
        $gallery_arr[] = array(
            'title' => $row['title'],
            'image' => $row['image']
        );
    }
    $result['status'] = "success";
    $result['message'] = "Details successfully retrieved.";
    $result['info'] = $info_arr;
    $result['gallery'] = $gallery_arr;
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable to retrieve resort details.";
}

echo json_encode($result);
?>