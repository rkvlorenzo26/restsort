<?php
include_once '../include_once/connection.php';

$result = array();

$query = "SELECT resort.resort_id, info.name, gallery.image, info.location, info.city_municipality ";
$query .= "FROM tbl_resort resort ";
$query .= "INNER JOIN tbl_resort_info info ON resort.resort_id = info.resort_id ";
$query .= "INNER JOIN tbl_resort_gallery gallery ON resort.resort_id = gallery.resort_id ";
$query .= "INNER JOIN tbl_resort_rates rates ON resort.resort_id = rates.resort_id ";
$query .= "WHERE resort.account_status = 'Active' GROUP BY resort.resort_id ORDER BY info.name ASC";

$load_resorts = mysqli_query($con, $query);

if (mysqli_num_rows($load_resorts) > 0) {
    $data = array();
    while ($row = mysqli_fetch_array($load_resorts)) {

        $address = $row['location'] .' '. $row['city_municipality'] . ' Cavite, Philippines';
        //$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true";
        // $request_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $address;
        // echo $request_url;

        $get_ratings_query = mysqli_query($con, "SELECT truncate(COALESCE(AVG(ratings), 0), 2) AS ratings FROM tbl_reviews WHERE resort_id = '$row[resort_id]'");
        $ratings = mysqli_fetch_array($get_ratings_query);

        $row_arr = array(
            'resort_id' => $row['resort_id'],
            'name' => $row['name'],
            'image' => $row['image'],
            'city_municipality' => $row['city_municipality'],
            'ratings' => $ratings['ratings']
        );
        array_push($data, $row_arr);
    }
    $result['status'] = "success";
    $result['data'] = $data;
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable fetch resorts data. Please try again.";
}

echo json_encode($result);
?>