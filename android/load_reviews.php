<?php
include_once '../include_once/connection.php';

$result = array();

if (isset($_POST['resort_id'])) {
    $id = $_POST['resort_id'];

    $query = "SELECT users.firstname, users.lastname, reviews.comment, reviews.ratings, reviews.image ";
    $query .= "FROM tbl_reviews reviews ";
    $query .= "JOIN tbl_users users ON reviews.user_id = users.user_id ";
    $query .= "WHERE reviews.resort_id = '". $id ."' ORDER BY date_reviewed DESC";

    $load_reviews = mysqli_query($con, $query);
    $data = array();
    while ($row = mysqli_fetch_array($load_reviews)) {
        $row_arr = array(
            'name' => $row['firstname'].' '.$row['lastname'],
            'comments' => $row['comment'],
            'ratings' => $row['ratings'],
            'image' => $row['image']
        );
        array_push($data, $row_arr);
    }
    $result['status'] = "success";
    $result['data'] = $data;
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable fetch reviews. Please try again.";
}

echo json_encode($result);
?>