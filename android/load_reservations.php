<?php
include_once '../include_once/connection.php';

$result = array();

if (isset($_POST['user_id'])) {
    $id = $_POST['user_id'];

    $query = "SELECT reservation.reservation_date_from, info.name, reservation.reservation_date_to, reservation.status, reservation.instructions ";
    $query .= "FROM tbl_resort_reservation reservation ";
    $query .= "JOIN tbl_resort_info info ON reservation.resort_id = info.resort_id AND reservation.user_id = '$id' ";
    $query .= "ORDER BY reservation.reservation_date_from ASC";

    $load_reviews = mysqli_query($con, $query);
    $data = array();
    while ($row = mysqli_fetch_array($load_reviews)) {
        $row_arr = array(
            'resortName' => $row['name'],
            'fromDate' => $row['reservation_date_from'],
            'toDate' => $row['reservation_date_to'],
            'instruction' => $row['instructions'],
            'status' => $row['status']
        );
        array_push($data, $row_arr);
    }
    $result['status'] = "success";
    $result['data'] = $data;
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable fetch reservations. Please try again.";
}

echo json_encode($result);
?>