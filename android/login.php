<?php
include_once '../include_once/connection.php';

$result = array();

if (isset($_POST['email']) && isset($_POST['password'])) {
    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $check = mysqli_query($con, "SELECT * FROM tbl_users WHERE email = '$email' AND password = '$password'");
    if (mysqli_num_rows($check) == 1) {
        $row = mysqli_fetch_array($check);

        if ($row['status'] == 'inactive') {
            $result['status'] = "failed";
            $result['message'] = "Please verify your account.";
        } else {
            $row_arr = array(
                'user_id' => $row['user_id'],
                'firstname' => $row['firstname'],
                'lastname' => $row['lastname'],
                'contact' => $row['contact_no'],
                'email' => $row['email'],
                'date_registered' => $row['date_registered']
            );
            $result['status'] = "success";
            $result['message'] = "Login successfully.";
            $result['data'] = $row_arr;
        }
    } else {
        $result['status'] = "failed";
        $result['message'] = "Invalid credentials. Please try again.";
    }
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable to login. Please try again.";
}

echo json_encode($result);
?>