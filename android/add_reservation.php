<?php
include_once '../include_once/connection.php';

$result = array();

if (isset($_POST['rate_id'])) {
    $user_id = $_POST['user_id'];
    $rate_id = $_POST['rate_id'];
    $reservation_date_from = date("Y-m-d", strtotime($_POST['reservation_date_from']));
    $reservation_date_to = date("Y-m-d", strtotime($_POST['reservation_date_to']));
    $instructions = $_POST['instructions'];

    $get_resort_id = mysqli_query($con, "SELECT * from tbl_resort_rates WHERE rate_id = '$rate_id'");
    $row = mysqli_fetch_array($get_resort_id);
    $resort_id = $row['resort_id'];

    $insert = mysqli_query($con, "INSERT INTO tbl_resort_reservation(resort_id, rate_id, user_id, status, reservation_date_from, reservation_date_to, instructions) 
                VALUES ('$resort_id', '$rate_id', '$user_id', 'Pending', '$reservation_date_from', '$reservation_date_to', '$instructions')");
    if ($insert) {
        $result['status'] = "success";
        $result['message'] = "Reservation successfully requested.";
    } else {
        $result['status'] = "failed";
        $result['message'] = "Unable to request reservation. Please try again.";
    }
} else {
    $result['status'] = "failed";
    $result['message'] = "Unable to request reservation. Please try again.";
}

echo json_encode($result);
?>