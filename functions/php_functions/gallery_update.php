<?php 
include_once '../../include_once/connection.php';

$target_dir = "../../uploads/gallery/";
$uploadOk = "";
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $id = $_POST['edit_id'];
    $title = $_POST['edit_title'];
    $description = $_POST['edit_description'];
    $target_file = $target_dir . time() . '_' . basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if(!file_exists($_FILES['fileToUpload']['tmp_name']) || !is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
        $update = mysqli_query($con, "UPDATE tbl_resort_gallery SET title = '$title', description = '$description' WHERE gallery_id = '$id'");
        if ($update) {
            $uploadOk = "success";
        } else {
            $uploadOk = "failed";
        }
    } else {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $name = time() . '_' .basename($_FILES["fileToUpload"]["name"]);
                $update = mysqli_query($con, "UPDATE tbl_resort_gallery SET image = '$name', title = '$title', description = '$description' WHERE gallery_id = '$id'");
                if ($update) {
                    $uploadOk = "success";
                } else {
                    $uploadOk = "failed";
                }
            } else {
                $uploadOk = "failed";
            }
        }
    }
}

header("location: ../../gallery.php?upload=".$uploadOk);
?>