<?php 
include_once '../../include_once/connection.php';

$target_dir = "../../uploads/gallery/";

$title = $_POST['title'];
$description = $_POST['description'];
$target_file = $target_dir . time() . '_' . basename($_FILES["fileToUpload"]["name"]);

$uploadOk = "";
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $name = time() . '_' .basename($_FILES["fileToUpload"]["name"]);
            $insert = mysqli_query($con, "INSERT INTO tbl_resort_gallery(resort_id, image, title, description) values ('$_SESSION[resort_id]', '$name', '$title', '$description')");
            if ($insert) {
                $uploadOk = "success";
            } else {
                $uploadOk = "failed";
            }
        } else {
            $uploadOk = "failed";
        }
    } else {
        $uploadOk = "failed";
    }
}

header("location: ../../gallery.php?upload=".$uploadOk);
?>