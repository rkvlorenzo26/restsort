<?php 
include_once '../../include_once/connection.php';

$id = $_POST['id'];
$status = $_POST['status'];
$reason = $_POST['reason'];
$updateOk = "";

$update = mysqli_query($con, "UPDATE tbl_resort_reservation set status = '$status', decline_reason = '$reason' WHERE id = '$id'");
if ($update) {

    $get_details = mysqli_query($con, "SELECT * FROM tbl_resort_reservation WHERE id = '$id'");
    $reservation_row = mysqli_fetch_array($get_details);
    $user_id = $reservation_row['user_id'];

    $get_user = mysqli_query($con, "SELECT * FROM tbl_users WHERE user_id = '$user_id'");
    $user_row = mysqli_fetch_array($get_user);

    $firstname = $user_row['firstname'];
    $email = $user_row['email'];
    $subj = '';
    $msg = '';
    $checker = False;
    if ($status == 'Approved') {
        $subj = 'Your reservation has been approved.';
        $msg = '
        <p>Hello '.$firstname.',</p>
        <p>Your reservation has been approved.</p>
        <p>Reservation details:</p>
        <p>Check-in date: '.$reservation_row['reservation_date_from'].'</p>
        <p>Check-out date: '.$reservation_row['reservation_date_to'].'</p>
        <p>Instructions: '.$reservation_row['instructions'].'</p>
        <p>Date posted: '.$reservation_row['posted_date'].'</p>
        ';
        $checker = True;
    } else if ($status = 'Declined') {
        $subj = 'Your reservation has been declined.';
        $msg = '
        <p>Hello '.$firstname.',</p>
        <p>Your reservation has been declined.</p>
        <p>Reservation details:</p>
        <p>Check-in date: '.$reservation_row['reservation_date_from'].'</p>
        <p>Check-out date: '.$reservation_row['reservation_date_to'].'</p>
        <p>Instructions: '.$reservation_row['instructions'].'</p>
        <p>Date posted: '.$reservation_row['posted_date'].'</p>
        <p>Decline reason: '.$reservation_row['decline_reason'].'</p>
        ';
        $checker = True;
    }

    if ($checker == True) {
        // use wordwrap() if lines are longer than 70 characters
        $msg = wordwrap($msg,70);
            
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        // send email
        mail($email, $subj, $msg, $headers);
    }

    $updateOk = "success";
} else {
    $updateOk = "failed";
}
header("location: ../../reservation.php?reservation=" . $updateOk);
?>