$(document).ready(function(){

  $('#login').click(function(){
    var username = $('#username').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var repassword = $('#retypepassword').val();
    var resort = $('#resort').val();
    var address = $('#address').val();
    var city_municipality = $('#city_municipality').val();

    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (username == "" || password == "" || repassword == "" || resort == "" || address == "" || email == "") {
      $('#modal-title').text('Registration Error');
      $('#modal-message').text('Please fill up all fields.');
      $('#modal-notification').modal('show');
    } else if (password != repassword) {
      $('#modal-title').text('Registration Error');
      $('#modal-message').text('Password not match.');
      $('#modal-notification').modal('show');
    } else if (!re.test(String(email).toLowerCase())) {
      $('#modal-title').text('Registration Error');
      $('#modal-message').text('Please enter valid email address');
      $('#modal-notification').modal('show');
    } else {
      var values = {
        "username" : username,
        "email" : email,
        "password" : password,
        "repassword" : repassword,
        "resort" : resort,
        "address" : address,
        "city_municipality" : city_municipality
      }

      $.ajax({
        url: 'functions/php_functions/registration.php',
        type: 'POST',
        data: values,
        success: function(message) {
            if (message == "success") {
              $('#modal-success').modal('show');
              // $(location).attr('href', 'index.php');
            } else {
              $('#modal-title').text('Registration Error');
              $('#modal-message').text(message);
              $('#modal-notification').modal('show');
            }
        }
      });
    }
  });

});