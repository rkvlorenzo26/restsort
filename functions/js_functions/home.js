$(document).ready(function(){
  loadOverview();
});

function loadOverview() {
  $.ajax({
    url: 'functions/php_functions/h_overview.php',
    type: 'POST',
    success: function(response) {
      console.log(response);
      var jsonObj = JSON.parse(response);
      $('#resort-name').text(jsonObj.info.name);
      $('#resort-location').text(jsonObj.info.location);
      $('#resort-info').text(jsonObj.info.description);
    }
  });
}