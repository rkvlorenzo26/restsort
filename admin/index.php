<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_admin_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tablePosts" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>CUSTOMER NAME</th>
                                                <th>CONTACT NO.</th>
                                                <th>EMAIL</th>
                                                <th>ACTIVATION CODE</th>
                                                <th>STATUS</th>
                                                <th>DATE REGISTERED</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_users = mysqli_query($con, "SELECT * FROM tbl_users ORDER BY date_registered DESC");
                                            while($row = mysqli_fetch_array($get_users))
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $row['firstname'] . " " . $row['lastname'];?></td>
                                                <td><?php echo $row['contact_no'];?></td>
                                                <td><?php echo $row['email'];?></td>
                                                <td><?php echo $row['code'];?></td>
                                                <td><?php echo $row['status'];?></td>
                                                <td><?php echo $row['date_registered'];?></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once 'include_once/js.php'?>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>