$(document).ready(function(){

  $('#login').click(function(){
    var username = $('#username').val();
    var password = $('#password').val();

    if (username == "" || password == "") {
      $('#modal-title').text('Login Error');
      $('#modal-message').text('Please fill up all fields.');
      $('#modal-notification').modal('show');
    } else {
      var values = {
        "username" : username,
        "password" : password
      }

      $.ajax({
        url: 'functions/php_functions/login.php',
        type: 'POST',
        data: values,
        success: function(message) {
            if (message == "success") {
              $(location).attr('href', 'index.php');
            } else if (message == "failed") {
              $('#modal-title').text('Login Error');
              $('#modal-message').text('Incorret username/password. Please try again.');
              $('#modal-notification').modal('show');
            }
        }
      });
    }
  });

});