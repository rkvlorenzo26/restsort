<?php 
include_once 'include_once/connection.php';
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/login/login-header.php'; ?>
<body>
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
                <img src="include_once/restsort.png" style="width: 150px; height: 150px; margin-bottom: 15px;"></br>
                ADMIN LOGIN
            </div>
            <div class="card-body">
                <div class="form-group">
                    <input class="form-control form-control-lg" id="username" name="username" type="text" placeholder="Username" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="password" name="password" type="password" placeholder="Password">
                </div>
                <button id="login" class="btn btn-lg btn-block btn-primary">Login</button>
            </div>
        </div>
    </div>
<?php include_once 'include_once/modal-alert.php'?>
<?php include_once 'include_once/login/login-scripts.php'; ?>
</body>
</html>