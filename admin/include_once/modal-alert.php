<div class="modal fade login" id="modal-notification">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header">
                <p id="modal-title"></p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            <p id="modal-message"></p>
            </div>
        </div>
    </div>
</div>