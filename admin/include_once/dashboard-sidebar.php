<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li>
                        <a class="nav-link" href="index.php"><i class="fa fa-fw fa-user"></i>Users</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="resorts.php"><i class="fa fa-fw fa-users"></i>Resorts</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="reviews.php"><i class="fa fa-fw fa-comment"></i>Reviews</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="functions/php_functions/logout.php"><i class="fa fa-fw fa-power-off"></i>Logout</a> 
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>