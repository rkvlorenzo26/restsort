<div class="modal fade login" id="modal-success">
    <div class="modal-dialog login animated">
        <div class="modal-content">
            <div class="modal-header">
                <p id="modal-title">Registration Successful</p>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            <p id="modal-message">Please make sure to add all information after logging in.</p>
            
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="index.php">Proceed to login page</a>
            </div>
        </div>
    </div>
</div>