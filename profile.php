<?php 
include_once 'include_once/connection.php';
if(isset($_SESSION['resort_logged_in']))
{
?>
<!doctype html>
<html lang="en">
<?php include_once 'include_once/head.php'?>
<body>
    <div class="dashboard-main-wrapper">
        <?php include_once 'include_once/dashboard-header.php'?>
        <?php include_once 'include_once/dashboard-sidebar.php'?>

        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content ">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="tablePosts" class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>TITLE</th>
                                                <th>DESCRIPTION</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $get_info = mysqli_query($con, "SELECT * FROM tbl_resort_info WHERE resort_id = '$_SESSION[resort_id]'");
                                            $row = mysqli_fetch_array($get_info);
                                            ?>
                                                <tr>
                                                    <td>Name</td>
                                                    <td><p id="display-name"><?php echo $row['name']; ?></p></td>
                                                    <td class="text-center">
                                                    <a href="javascript:void(0)" onclick="action('name')" data-toggle="tooltip" data-placement="left" title="Edit" id="edit" name="edit"><span class="fa fa-edit"></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Location</td>
                                                    <td><p style="display: inline-block;" id="display-location"><?php echo $row['location']; ?></p>, <?php echo $row['city_municipality']; ?>, Cavite</td>
                                                    <td class="text-center">
                                                    <a href="javascript:void(0)" onclick="action('location')" data-toggle="tooltip" data-placement="left" title="Edit" id="edit" name="edit"><span class="fa fa-edit"></span></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td><p id="display-description"><?php echo $row['description']; ?></p></td>
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" onclick="action('description')" data-toggle="tooltip" data-placement="left" title="Edit" id="edit" name="edit"><span class="fa fa-edit"></span></a>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login" id="editModal">
        <div class="modal-dialog login animated">
            <div class="modal-content">
                <div class="modal-header">
                    <p id="confimHeader">Edit Description</p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="functions/php_functions/profile_edit.php" method="POST">
                        <div id = "formName">
                            Resort Name:
                            <input class="form-control" type="text" id="name" name="name">
                        </div>

                        <div id = "formDesc">
                            Description:
                            <textarea class="form-control" rows="10" id="description" name="description"></textarea>
                        </div>

                        <div id = "formLocation">
                            Resort Address:
                            <input class="form-control" type="text" id="address" name="address">
                            City/Municipality:
                            <select class="form-control" id="city_municipality" name="city_municipality">
                            <?php
                                $get_city_municipality = mysqli_query($con, "SELECT * FROM tbl_location ORDER BY name ASC");
                                $get_info = mysqli_query($con, "SELECT * FROM tbl_resort_info WHERE resort_id = '$_SESSION[resort_id]'");
                                $info = mysqli_fetch_array($get_info);
                                while ($row = mysqli_fetch_array($get_city_municipality)) {
                                    if ($info['city_municipality'] == $row['name']) { ?>
                                        <option value = "<?php echo $row['name']; ?>" selected><?php echo $row['name']; ?></option>
                            <?php   } else  {   ?>
                                        <option value = "<?php echo $row['name']; ?>"><?php echo $row['name']; ?></option>
                            <?php   }   }   ?>
                        </select>
                        </div>

                    <input type="hidden" id="action" name="action">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php include_once 'include_once/js.php'?>

    <script>
        function action(action) {
            $('#formName').hide();
            $('#formDesc').hide();
            $('#formLocation').hide();

            if (action == "name") {
                var name = $('#display-name').text();
                $('#name').val(name);
                $('#confimHeader').text("Edit Name");
                $('#action').val(action);
                $('#formName').show();
                $('#editModal').modal('show');
            } else if (action == "location") {
                var address = $('#display-location').text();
                $('#confimHeader').text("Edit Location");
                $('#action').val(action);
                $('#address').val(address);
                $('#formLocation').show();
                $('#editModal').modal('show');
            } else if (action == "description") {
                var description = $('#display-description').text();
                $('#confimHeader').text("Edit Description");
                $('#action').val(action);
                $('#description').val(description);
                $('#formDesc').show();
                $('#editModal').modal('show');
            }
        }
    </script>
</body>
</html>
<?php
} else {
 header("location: login.php");
}
?>