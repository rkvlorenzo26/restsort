<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li>
                        <a class="nav-link" href="index.php"><i class="fa fa-fw fa-home"></i>Overview</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="profile.php"><i class="fa fa-fw fa-user"></i>Profile</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="gallery.php"><i class="fa fa-fw fa-image"></i>Gallery</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="rates.php"><i class="fab fa-fw fa-wpforms"></i>Rates</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="reservation.php"><i class="fa fa-fw fa-calendar"></i>Reservation</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="reviews.php"><i class="fa fa-fw fa-comment"></i>Reviews</a> 
                    </li>
                    <li>
                        <a class="nav-link" href="functions/php_functions/logout.php"><i class="fa fa-fw fa-power-off"></i>Logout</a> 
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>